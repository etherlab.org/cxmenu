#!/usr/bin/python3

# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

import time as t
import struct
import selectors


# ----------------------------------------------------------------------------
# Prepare handling of input device
MESSAGE_FORMAT = '=lhBB'
EVENT_SIZE = struct.calcsize(MESSAGE_FORMAT)
DEVICE_PATH_BTN = "/dev/input/js0"
dfBTN = open(DEVICE_PATH_BTN, "rb")
# fd = os.open(DEVICE_PATH, os.O_NONBLOCK)
# df = os.fdopen(fd, "rb")



# ----------------------------------------------------------------------------
# Read information out device file
def readbuttons():
    event = dfBTN.read(EVENT_SIZE)
    [timeStamp, value, type, number] = struct.unpack(MESSAGE_FORMAT, event)

    print(timeStamp)
    print(value, type, number, "\n")


# ----------------------------------------------------------------------------
# Prepare selector handling
select = selectors.DefaultSelector()
select.register(dfBTN, selectors.EVENT_READ, readbuttons)



# ----------------------------------------------------------------------------
# Infinity Loop

starttime = t.time()

while 1:

    # --------- Excecution without selector and callback
    # readbuttons()
    # event = df.read()
    # print(event)

    events = select.select(1)
    for key, mask in events:
        callback = key.data
        callback()

    gonetime = t.strftime('%H:%M:%S', t.gmtime(t.time() - starttime))
    print(gonetime)


#
