#!/usr/bin/python3

# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

import time as t

# ----------------------------------------------------------------------------
# Prepare handling of output device
DEVICE_PATH_DSP = "/dev/cx_display"
dfDSP = open(DEVICE_PATH_DSP, 'w')



# ----------------------------------------------------------------------------
# Write information to device file
def writedisplay(output):
    dfDSP.write('\f')
    dfDSP.write(output + "\n" + "                ")
    dfDSP.flush()


# ----------------------------------------------------------------------------
# Infinity Loop

starttime = t.time()

while 1:
    gonetime = t.strftime('%H:%M:%S', t.gmtime(t.time() - starttime))

    print(gonetime)
    writedisplay(gonetime)
