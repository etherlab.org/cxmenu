#! /usr/bin/python3

# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

import curses

shell = curses.initscr()
# shell.timeout(100)
curses.noecho()
curses.cbreak()

x = 0
counter = 0

try:
    while 1:
        input = shell.getch()
        counter += 1

        shell.addstr(1, 0, 'Sandbox for curses.')
        shell.addstr(2, 0, '-------------------')
        shell.addstr(3, 0, 'Key: ')
        shell.refresh()
        shell.move(3, x)

        if input != -1:
            shell.addstr(3, 0, ' ' * 10)
            output = 'Key: ' + str(input) + ' ' + str(counter)
            shell.addstr(3, 0, output)
            x = len(output) + 1
            shell.move(3, x)
            shell.refresh()



except KeyboardInterrupt:
    curses.nocbreak()
    curses.echo()
    curses.endwin()
    exit(0)

except Exception as e:
    curses.nocbreak()
    curses.echo()
    curses.endwin()

    print(e)
    exit(0)
