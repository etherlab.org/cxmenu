#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------





# ------------------------- Loading Necessary Modules -------------------------
import pdcom
import selectors
from socket import socket, AF_INET, SOCK_STREAM
import os
import time as t


# =============================================================================
class Process(pdcom.Process):
    def __init__(self, addr, connectedCallback=None, sel=None):
        self.addr = addr
        pdcom.Process.__init__(self)
        self.connectedCallback = connectedCallback

        self.socket = None
        self.fo = None

        self.selector = sel
        self.TIMEOUT = 5  # seconds
        self.nextCall = t.time()

        self.connect()


    # -------------------------------------------------------------------------
    def connect(self):
        """Connect the socket of realtime process."""

        print(f'Connect the process {self.addr}.')
        try:
            self.socket = socket(AF_INET, SOCK_STREAM)
            self.socket.connect(self.addr)

        except:
            print(f'Failed to connect to {self.addr}.')
            self.connloss()
            return


        print(f'Connected to {self.addr}, fileno is {self.socket.fileno()}.')
        self.fo = os.fdopen(self.socket.fileno(), "wb")

        if self.selector:
            print(f'Registering fd {self.fo} at selector.')
            self.selector.register(self.fo, selectors.EVENT_READ,
                    self.dataToRead)

        self.nextCall = None



    # -------------------------------------------------------------------------
    def run(self):
        """Callback method for the selector.

        This method checks if the call by the selector is ment for this object.
        Only if the time accords to the set timestamp the connect method gets
        called."""

        if self.nextCall and t.time() >= self.nextCall:
            self.connect()


    # -------------------------------------------------------------------------
    def findReply(self, var):
        findCallback(var)



    # -------------------------------------------------------------------------
    def dataToRead(self):
        """Callback method for the selector."""

        if prcs.asyncData() <= 0:
            print('Connection is interrupted...')
            self.connloss()



    # -------------------------------------------------------------------------
    def connloss(self):
        """Determine the timestamp for the next call and resets
        the selector."""

        self.nextCall = t.time() + self.TIMEOUT
        print(f'Try to (re)connect at:',
        t.strftime('%H:%M:%S', t.gmtime(self.nextCall)))

        if self.selector and self.fo:
            try:
                print(f'Unregistering fd {self.fo} from selector.')
                self.selector.unregister(self.fo)

            except:
                pass

        if self.fo:
            self.fo.close()
            self.fo = None

        self.socket = None
        self.reset()



    # -------------------------------------------------------------------------
    def connected(self):
        """Reimplemented from pdcom.Process (mandatory)

        This signal is received when protocol initialization has completed.
        """
        print('Process is connected.')
        if self.connectedCallback:
            self.connectedCallback()



    # -------------------------------------------------------------------------
    def read(self, n):
        """Reimplemented from pdcom.Process (mandatory).

        Fetch data from input stream.
        """
        return self.socket.recv(n)



    # -------------------------------------------------------------------------
    def write(self, buf):
        """Reimplemented from pdcom.Process (mandatory)

        Send buf to output stream;
        Return number of bytes written, which MUST be the whole buffer.

        It is wise to use buffered output because the the handler calls
        write() very often.
        """

        self.fo.write(buf)
        return len(buf)



    # -------------------------------------------------------------------------
    def flush(self):
        """Reimplemented from pdcom.Process (mandatory)

        Flush output stream.
        Return 0 on success
        """

        self.fo.flush()
        return 0  # Flush is always successful.





# =============================================================================
class Task(pdcom.Subscriber):
    def __init__(self, sampleTime):
        pdcom.Subscriber.__init__(self)
        self.exec = None
        self.period = None
        self.sampleTime = sampleTime
        self.util = 0
        self.lpUtil = 0
        self.tau = 3.0
        self.counter = 0

    # -------------------------------------------------------------------------
    def lowpass(self, util):
        self.lpUtil += self.sampleTime / self.tau * (util - self.lpUtil)
        self.counter += 1
        if self.counter % 1000 == 0:
            print('Util:', util, self.lpUtil)



    # -------------------------------------------------------------------------
    def newGroupValue(self, time):
        # print("New Group-Value:", ", ".join(
        #         [str(s.getValue()) for s in self.subscription]))

        if self.exec and self.period and self.period.getValue() > 0:
            self.util = self.exec.getValue() / self.period.getValue()
            self.lowpass(self.util)


    # -------------------------------------------------------------------------
    def newValue(self, subscpt):
        """Mandatory"""
        pass
        # print("New Value:", subscpt, subscpt.getValue())


    # -------------------------------------------------------------------------
    def active(self, path, subscpt):
        print(path, "is active")

        if path.endswith('Period'):
            self.period = subscpt

        else:
            self.exec = subscpt



    # -------------------------------------------------------------------------
    def invalid(self, process, path, id):
        # print(process, path, id, "is invalid.")
        self.exec = None
        self.period = None


# =============================================================================
# Main function of unittest

"""Unittest for pdcom interface to read a specified realtime process.

In this case the the utilization of the process is calculated for all existing
ones. For further information have a look in the original library pdcom by IgH.
"""

# -----------------------------------------------------------------------------
def connectedCallback():
    """Callback method can bind the subscriber to the process."""
    prcs.find('/Taskinfo/%u/Period' % (taskidx))
    # Variable gibt es, oder nicht.



# -----------------------------------------------------------------------------
def findCallback(var):
    global taskidx, tasks

    print(repr(var))

    if var:
        taskidx += 1
        tasks.append(Task(var.sampleTime))
        prcs.find('/Taskinfo/%u/Period' % (taskidx))
        # Variable gibt es, oder nicht.

    else:
        for idx in range(taskidx):
            prcs.subscribe(tasks[idx], '/Taskinfo/%u/Period' % (idx),
                    tasks[idx].sampleTime, 1)
            prcs.subscribe(tasks[idx], '/Taskinfo/%u/ExecTime' % (idx),
                    tasks[idx].sampleTime, 1)



# -----------------------------------------------------------------------------
def getUtil():
    global tasks
    cumUtil = 0

    for idx in tasks:
        cumUtil += tasks[idx].lpUtil

    print(cumUtil)
    return cumUtil



# -----------------------------------------------------------------------------
def nextEventtime():
    """Return the timestamp to call the next event."""
    if prcs.nextCall is None:
        return 10  # Stdandard timeout in application

    else:
        return prcs.nextCall


# ----------------------------- Implement Selector ----------------------------
selector = selectors.DefaultSelector()



# ----------------------- Create Process and Subscriber -----------------------
taskidx = 0
tasks = []

prcs = Process(('localhost', 2345), connectedCallback=connectedCallback,
    sel=selector)


# --------------------------------- Main Loop ---------------------------------
while True:

    events = selector.select(nextEventtime() - t.time())
    for key, mask in events:
        callback = key.data
        callback()

    prcs.run()

#
