#!/usr/bin/python3

# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

import time as t
import struct
import os

fd = os.open("/dev/input/js0", os.O_NONBLOCK)
dfBTN = os.fdopen(fd, "rb")

MESSAGE_FORMAT = '=lhBB'
EVENT_SIZE = struct.calcsize(MESSAGE_FORMAT)

dfDSP = open("/dev/cx_display", 'w')


# ----------------------------------------------------------------------------
starttime = t.time()

while 1:
    event = dfBTN.read(EVENT_SIZE)
    print(event)
    if event:
        [timeStamp, value, type, number] = struct.unpack(MESSAGE_FORMAT, event)
        print(value, type, number, "\n")

    gonetime = t.strftime('%H:%M:%S', t.gmtime(t.time() - starttime))
    print(gonetime)

    dfDSP.write('\f')
    dfDSP.write(gonetime + "\n" + "                ")
    dfDSP.flush()

    t.sleep(0.1)
