# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
from enum import Enum
import logging

from cxmenutools import globals as glb
from cxmenutools.globals import eventtype
from cxmenutools.page import Page



# -------------------------- Enum to Name the Layer ---------------------------
class layer(Enum):
    THIS = 0
    SUB = 1


# -----------------------------------------------------------------------------
class Menu(Page):
    """cxmenu application
    Module:      menue

    This module controls the output for a menu. The main menu, as well as
    possible submenues of the modules. The main menu is build directly from a
    list out off the modules, submenues have do be build manually in the
    constructor of the module.
    """


    # -------------------------------------------------------------------------
    def __init__(self, parent, entries):
        self.parent = parent
        self.index = 0
        self.cursoridx = 0

        self.log = logging.getLogger('cxmenu.' + __name__)
        self.log.setLevel(logging.DEBUG)


        self.back = None  # Gets a reference to a method.
        self.menulayer = layer.THIS


        # -------------- Create Outputlist with Headerstatements --------------
        self.headlist = []
        self.cursor = ["  ", "> "]

        self.entries = glb.toList(entries)
        for entry in self.entries:
            self.headlist.append(entry.getTitle())


    # -------------------------------------------------------------------------
    # This two methods are to set the current menue layer back. Therefore the
    # menu object has to give it's own "back-" layer to the next menu. This own
    # "back-" layer is given in the method ownback.
    def setBack(self, back):
        self.back = back

    def ownback(self):
        self.menulayer = layer.THIS



    # -------------------------------------------------------------------------
    # Whenever a menu or a submenu is entered the cursers have to be resetted
    # and the back-layer has to be set.
    def enter(self, backlayer):
        self.setBack(backlayer)
        self.cursoridx = 0
        self.index = 0



    # -------------------------------------------------------------------------
    # This method controls the walk through the menu and it's options. If the
    # menu is in this (own) layer it controls the current buttonevents, other-
    # wise the button event is passed to the menu of the next layer.
    def buttonevent(self, event):
        # ---------- Pass the event to submenu or to tip
        if self.menulayer == layer.SUB and self.selectedEntry().submenu:
            self.selectedEntry().submenu.buttonevent(event)

        elif self.menulayer == layer.SUB:
            self.selectedEntry().buttonevent(event)


        # ---------- Go one layer backwoard.
        elif event == eventtype.DOWN and self.back:
            self.exit()


        # ---------- Go one layer forward
        elif event == eventtype.ENTER or event == eventtype.UP:
            if self.selectedEntry().submenu:
                submenu = self.selectedEntry().submenu
                self.menulayer = layer.SUB
                submenu.enter(self.ownback)

            else:
                self.selectedEntry().buttonevent(event)


        # ---------- Switch entry in menulayer
        elif event == eventtype.RIGHT:
            self.skip(1)

        elif event == eventtype.LEFT:
            self.skip(-1)



    # -------------------------------------------------------------------------
    # Sets the index of the headlist and the cursor on the correct position.
    # There is to differ between the current cursor position on the upper or
    # lower line and the movent of the lists.
    def skip(self, step):

        # ---------- Step backwoards in the list (go to prev. line) -----------
        if step < 0:
            if self.cursoridx == 0:
                if self.index == 0:
                    pass  # TODO: Add some overrun.

                else:
                    self.index -= 1

            else:
                self.cursoridx -= 1

        # ----------- Step forwards in the list (go to next. line) ------------
        elif step > 0:
            if self.cursoridx >= glb.DSP_NUMLINES - 1:
                if self.index >= len(self.headlist) - glb.DSP_NUMLINES:
                    pass  # TODO: Add some overrun.

                else:
                    self.index += 1

            else:
                self.cursoridx += 1



    # -------------------------------------------------------------------------
    def selectedEntry(self):
        return self.entries[self.index + self.cursoridx]



    # -------------------------------------------------------------------------
    def getOutput(self):
        if self.menulayer == layer.SUB:
            if self.selectedEntry().submenu:
                return self.selectedEntry().submenu.getOutput()
            else:
                return self.selectedEntry().getOutput()

        elif len(self.headlist) == 1:
            line1 = self.headlist[0]
            line2 = "                "
            return [line1, line2]

        else:
            line1 = self.cursor[self.cursoridx == 0] + \
                self.headlist[self.index]
            line2 = self.cursor[self.cursoridx == 1] + \
                self.headlist[self.index + 1]
            return [line1, line2]



    # -------------------------------------------------------------------------
    def resetEntryLayer(self):
        if self.selectedEntry().submenu:
            self.selectedEntry().submenu.exit()


    # -------------------------------------------------------------------------
    def exit(self):
        # Calls the passed back-method. (See also self.back in init)
        if self.back:
            self.back()




#
