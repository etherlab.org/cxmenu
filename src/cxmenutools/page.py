# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ---------------------------- Loading Own Modules ----------------------------
from cxmenutools import globals as glb


# -----------------------------------------------------------------------------
class Page():
    """cxmenu application
    Module:      page

    This module contains all information and method of a page. A
        page is something representing. For excample a menu, a submenu, a
        single entry of a menu, or an interacting entry.
    """

    # -------------------------------------------------------------------------
    def __init__(self, parent, output):
        self.main = glb.getMainRef()
        self.parent = parent
        self.output = glb.toList(output)
        self.title = self.output[0]
        self.submenu = None  # For check existence.



    # -------------------------------------------------------------------------
    def enter(self):
        pass



    # -------------------------------------------------------------------------
    def exit(self):
        pass



    # -------------------------------------------------------------------------
    def getTitle(self):
        return self.title



    # -------------------------------------------------------------------------
    def getOutput(self):
        return self.output



    # -------------------------------------------------------------------------
    def buttonevent(self, event):
        pass
