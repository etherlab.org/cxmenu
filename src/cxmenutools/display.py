# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
import logging
from os import path

from cxmenutools import option as opt



# -----------------------------------------------------------------------------
class Display():
    """cxmenu application
    Module:      display

    This is the interface between the output by the different modules and the
    device file of the display or a terminal. The device file represents the
    output directly to the display. Alternatively an output on a terminal is
    possible. For each option an own class is given.
    """


    # -------------------------------------------------------------------------
    DEVICE_PATH = "/dev/cx_display"



    # -------------------------------------------------------------------------
    def __init__(self, parent):
        self.parent = parent
        self.log = logging.getLogger('cxmenu.' + __name__)

        # Check if device file exists. This can't be made with the open
        # argument, because the file will be opend for writing. Therefore
        # root rights are necessary.
        if not path.exists(self.DEVICE_PATH) and opt.remote:
            self.log.debug('Devicefile for display not found.')
            self.log.debug('Application goes to dryrun without CX21xx.')
            opt.dryrun = True


        # ------------------- Handling of the Input Device --------------------
        if not opt.dryrun:
            try:
                self.df = open(self.DEVICE_PATH, 'w')

            except FileNotFoundError as e:
                if opt.remote:
                    self.parent.shell.closeRemoteDisplay()

                print(e)
                print("""Make shure, that all modules for the BIOS API are loaded.
                You can install an load these manually by executing the
                shell-script.
                installBBAPI""")
                quit(1)

            except PermissionError as e:
                if opt.remote:
                    self.parent.shell.closeRemoteDisplay()

                print(e)
                print("""Try to execute as root.
                To run the application without a CX21xx you can start the
                remote mode:
                cxmenu -r""")
                quit(1)



    # -------------------------------------------------------------------------
    def write(self, output):
        self.log.info('%s \\n %s', output[0], output[1])

        # Prevent double time "\n".
        if output[0] == "\n":
            output[0] = "                "

        if not opt.dryrun:
            self.df.write('\f')
            self.df.write(output[0] + "\n" + output[1])
            self.df.flush()
