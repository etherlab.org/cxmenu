# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
from cxmenutools import globals as glb
from cxmenutools import option as opt

import curses
import logging
import time as t



# -----------------------------------------------------------------------------
class RemoteDisplay():
    """cxmenu application
    Module:      remotedisplay

    This class contains all methods to open an own shell and interacts with
    this. It also prints the verbose output.
    """

    TIMEOUT = 0.5  # sec.


    # -------------------------------------------------------------------------
    def __init__(self, parent, loghandler):
        # To create box drawings use ctrl shift u <xxxx> enter
        # for the drawing numbers have a look on:
        # https://jrgraphix.net/r/Unicode/2500-257F

        self.parent = parent

        # ---------- Common for logging
        self.loghandler = loghandler
        self.log = logging.getLogger('cxmenu.' + __name__)
        self.log.setLevel(logging.DEBUG)
        self.log.debug('Started remote mode. To quit press \'q\'')
        self.log.debug('')

        # ---------- Set Callback to write complete log-buffer
        self.parent.registerCallback(self, self.output)
        self.parent.registerCallback(self, self.initButtonSim)
        self.nextCall = t.time()  # To init Buttons

        # ---------- Init for curses
        self.curser = 0
        self.prompt = "cxmenu>"

        self.shell = curses.initscr()
        self.shell.timeout(100)
        curses.start_color()
        curses.curs_set(0)
        curses.noecho()
        curses.cbreak()  # Enables input via keyboard without confirming enter.

        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
        curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_RED)
        curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_WHITE)

        # ---------- Header and CX20xx display simulation
        self.shell.addstr(2, 0, ' ' * 70, curses.color_pair(2))
        self.shell.addstr(2, 2, "BECKHOFF BIOS API - by IgH Essen.",
                curses.color_pair(2))

        # ---------- Create white box, buttons and information text
        for y in range(3, 9):
            self.shell.addstr(y, 0, ' ' * 70, curses.A_STANDOUT)

        self.initButtonSim()
        self.shell.addstr(4, 32, 'To emulate the release of',
                curses.A_STANDOUT)
        self.shell.addstr(5, 32, 'enter press backspace.', curses.A_STANDOUT)


        # ---------- Window for the terminal oputput
        self.hight = curses.LINES - 10
        self.width = curses.COLS - 1
        self.terminal = curses.newwin(self.hight, self.width, 10, 0)



    # -------------------------------------------------------------------------
    def closeRemoteDisplay(self):
        curses.nocbreak()
        curses.echo()
        curses.endwin()



    # -------------------------------------------------------------------------
    def getShell(self):
        return self.shell



    # -------------------------------------------------------------------------
    def getNextEventtime(self):
        return self.nextCall



    # -------------------------------------------------------------------------
    def setDryrunInfo(self):
        if opt.dryrun:
            self.shell.addstr(7, 32, '⇄ OFFLINE - No connection to CX21xx.',
                    curses.color_pair(1))

        else:
            self.shell.addstr(7, 32, '⇄ ONLINE',
                    curses.color_pair(3))


    # -------------------------------------------------------------------------
    def output(self, event = None):
        """Gets called if the main modul needs an output to the shell."""

        out = self.loghandler.readPuffer()
        for line in out:
            line = line.format()
            logMessage = line.split(' - ')

            if logMessage[0] == 'INFO':

                if logMessage[2].startswith('keyevent'):
                    # Create button simulator
                    btnEvent = logMessage[2].split(':')

                    if btnEvent[1] in ['65', 'UP']:
                        self.shell.addstr(4, 27, '▲', curses.color_pair(1))

                    elif btnEvent[1] in ['66', 'DOWN']:
                        self.shell.addstr(6, 27, '▼', curses.color_pair(1))

                    elif btnEvent[1] in ['67', 'RIGHT']:
                        self.shell.addstr(5, 29, '▶', curses.color_pair(1))

                    elif btnEvent[1] in ['68', 'LEFT']:
                        self.shell.addstr(5, 25, '◀', curses.color_pair(1))

                    elif btnEvent[1] in ['10', 'ENTER']:
                        self.shell.addstr(5, 27, '■', curses.color_pair(1))

                    if not self.nextCall:
                        self.parent.registerCallback(self, self.initButtonSim)
                        self.nextCall = t.time() + glb.CALLBACKTIMEOUT

                else:
                    # Create display simulator
                    outstr = logMessage[2].split(' \\n ')

                    self.shell.addstr(4, 3, "┌────────────────┐",
                        curses.A_STANDOUT)
                    self.shell.addstr(5, 3, "│                │",
                        curses.A_STANDOUT)
                    self.shell.addstr(5, 4, outstr[0], curses.A_STANDOUT)
                    self.shell.addstr(6, 3, "│                │",
                        curses.A_STANDOUT)
                    self.shell.addstr(6, 4, outstr[1], curses.A_STANDOUT)
                    self.shell.addstr(7, 3, "└────────────────┘",
                        curses.A_STANDOUT)


            elif logMessage[0] == 'DEBUG' and opt.verbose:
                outstr = " " + self.prompt + " " + logMessage[2]
                self.terminal.addstr(self.curser, 0, outstr)

                if self.curser < self.hight - 2:
                    self.curser += 1
                else:
                    self.terminal.clear()
                    self.curser = 0

            # Refresh the output.
            self.shell.refresh()
            self.terminal.refresh()



    # -------------------------------------------------------------------------
    def initButtonSim(self):
        """(Re-)inits the simulated buttons."""

        if self.nextCall and t.time() >= self.nextCall:
            self.shell.addstr(5, 27, '■', curses.A_STANDOUT)
            self.shell.addstr(4, 27, '▲', curses.A_STANDOUT)
            self.shell.addstr(5, 25, '◀', curses.A_STANDOUT)
            self.shell.addstr(5, 29, '▶', curses.A_STANDOUT)
            self.shell.addstr(6, 27, '▼', curses.A_STANDOUT)
            self.shell.refresh()

            self.parent.releaseCallback(self, self.initButtonSim)
            self.nextCall = None




#
