# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ---------------------------- Loading Own Modules ----------------------------
from cxmenutools import globals as glb
from cxmenutools.page import Page

import time as t


# -----------------------------------------------------------------------------
class ProgressAction(Page):
    """cxmenu application
    Module:      progressaction

    This module controls interaction with an user that has timed progress.
    """

    # -------------------------------------------------------------------------
    def __init__(self, parent, output):
        self.main = glb.getMainRef()
        self.parent = parent
        self.output = glb.toList(output)
        self.submenu = None  # For check existence.
        self.isActive = False
        self.draw = None



    # -------------------------------------------------------------------------
    def enter(self, totaltime, drawKind):
        # Timer to control progress
        self.starttime = t.time()
        self.totaltime = totaltime
        self.draw = drawKind



    # -------------------------------------------------------------------------
    def setActive(self):
        self.isActive = True



    # -------------------------------------------------------------------------
    def interrupt(self):
        self.isActive = False



    # -------------------------------------------------------------------------
    def exit(self):
        self.main.settimer(glb.MENUTIMEOUT)
        self.isActive = False



    # -------------------------------------------------------------------------
    def getGoneTime(self):
        return t.time() - self.starttime



    # -------------------------------------------------------------------------
    def getOutput(self):
        if self.getGoneTime() <= self.totaltime:
            return self.draw(self.getGoneTime())



    # -------------------------------------------------------------------------
    def drawProgressbar(self, gonetime):
        """Draws a progressbar like the following
        example: [###........] 4s."""
        length = glb.DSD_NUMCHARS - 2
        filled = round(length * gonetime / self.totaltime)

        bar = '#' * filled + '.' * (length - filled)

        return '[' + bar + ']'



    # -------------------------------------------------------------------------
    def drawTimeout(self, gonetime):
        return '[' + t.strftime('%S', t.gmtime(self.totaltime - gonetime)) + \
            ']'


#
