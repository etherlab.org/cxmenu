# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
from cxmenutools import globals as glb
from cxmenutools.globals import eventtype
from cxmenutools.globals import eventhandle
from cxmenutools.page import Page
from cxmenutools import progressaction as pact
from cxmenutools import menu

import os
import time as t


# -----------------------------------------------------------------------------
class systemaction(Page):
    """cxmenu application
    Module:      systemaction

    This module allows to make system commands. To ensure that no unwanted
    commands are sent this class checks the duration of an enter event.
    """


    # -------------------------------------------------------------------------
    def __init__(self, parent, output, command):
        self.main = glb.getMainRef()
        self.parent = parent
        self.output = glb.toList(output)
        self.title = self.output[0]
        self.submenu = None
        self.message = ['\f', '\f']

        self.command = command
        self.action = pact.ProgressAction(self, None)

        self.HOLDTIME = 3
        self.RELEASETIME = 3
        self.handle = None

        self.nextCall = None



    # -------------------------------------------------------------------------
    def getNextEventtime(self):
        return self.nextCall



    # -------------------------------------------------------------------------
    def buttonevent(self, event):
        if event == eventtype.DOWN:
            # Go one layer backwoard.
            self.exit()

        elif event == eventtype.ENTER and self.action.isActive:
            # Start action countdown
            self.action.enter(self.HOLDTIME, self.action.drawProgressbar)
            self.main.registerCallback(self, self.handleAction)
            self.nextCall = t.time() + glb.CALLBACKTIMEOUT

        elif event in [eventtype.ENTER, eventtype.UP] \
                or not self.action.isActive:
            # Abort action
            self.parent.submenu.menulayer = menu.layer.SUB
            self.output = [self.getTitle(), "<HOLD ENTER>"]
            self.action.setActive()
            self.handle = eventhandle.HOLD



    # -------------------------------------------------------------------------
    def handleAction(self):
        if not self.nextCall:
            return

        event = self.main.getEvent()

        # Hold to reach next Step
        if self.handle == eventhandle.HOLD:
            # Keep holding
            if self.action.getGoneTime() <= self.action.totaltime \
                    and event == eventtype.ENTER:
                self.output = [self.getTitle(), self.action.getOutput()]

            # Held long enough
            elif self.action.getGoneTime() > self.action.totaltime \
                    and event == eventtype.ENTER:
                self.action.enter(self.RELEASETIME, self.action.drawTimeout)
                self.handle = eventhandle.RELEASE

            # Released too early
            elif self.action.getGoneTime() <= self.action.totaltime \
                    and event == eventtype.ENTER_R:
                self.action.interrupt()
                self.buttonevent(event)

        # Release to acknowledge next Step
        elif self.handle == eventhandle.RELEASE:
            # Released in time
            if event == eventtype.ENTER_R \
                    and self.action.getGoneTime() <= self.action.totaltime:
                self.main.dsp.write(self.message)
                os.system(self.command)

            # Waiting for release
            elif event == eventtype.ENTER \
                    and self.action.getGoneTime() <= self.action.totaltime:
                self.output = ["<RELEASE>", self.action.getOutput()]

            # Released to late
            elif event == eventtype.ENTER \
                    and self.action.getGoneTime() > self.action.totaltime:
                self.output = ["<ABORT>", "\n"]

            # Waiting for releasing and ignoring input
            elif event == eventtype.ENTER_R \
                    and self.action.getGoneTime() > self.action.totaltime:
                self.action.exit()
                self.main.releaseCallback(self, self.handleAction)
                self.nextCall = None
                self.buttonevent(event)



    # -------------------------------------------------------------------------
    def setMessage(self, message):
        self.message = message



    # -------------------------------------------------------------------------
    def getOutput(self):
        return self.output



    # -------------------------------------------------------------------------
    def exit(self):
        self.parent.submenu.menulayer = menu.layer.THIS
        self.action.exit()

        if self.action.isActive:
            self.main.releaseCallback(self, self.handleAction)
