# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Further information:
# Using the joistic api to evaluate the pressed, or released buttons on the
# CX 21xx. Get more information on:
# https://www.kernel.org/doc/Documentation/input/joystick-api.txt
# -----------------------------------------------------------------------------



# ------------------------- Loading Necessary Modules -------------------------
import logging
import selectors
import struct

from cxmenutools.globals import eventtype
from cxmenutools import option as opt


# --------------------------- Defining Event Codes ----------------------------
EVENT_BUTTON = 0x01
EVENT_AXIS = 0x02
LEFT_RIGHT = 0x00
TOP_DOWN = 0x01



# -----------------------------------------------------------------------------
class Button():
    """cxmenu application
    Module:      button

    This is the module to read ou24.06.2020t the device file of the buttons, in
    this case an joystick. The infomration out of the file are interpreted as
    direction- and an enter-buttons. It sends an interrupt signal to the main
    Module if some input is recognized.
    """

    MESSAGE_FORMAT = '=lhBB'
    EVENT_SIZE = struct.calcsize(MESSAGE_FORMAT)
    DEVICE_PATH = "/dev/input/js0"
    i = 1



    # -------------------------------------------------------------------------
    def __init__(self, parent):
        self.parent = parent
        self.selector = parent.select

        self.log = logging.getLogger('cxmenu.' + __name__)
        self.log.setLevel(logging.DEBUG)


        # ------------------- Handling of the Input Device --------------------
        try:
            self.df = open(self.DEVICE_PATH, "rb")


        except FileNotFoundError as e:
            if opt.remote:
                self.log.debug('Devicefile for buttons not found.')
                self.log.debug('Application goes to dryrun without CX21xx.')
                opt.dryrun = True

            else:
                print(e)
                print("""Make shure, that all modules for the BIOS API are loaded.
                You can install an load these manually by executing the
                shell-script.
                installBBAPI""")
                quit(1)


        except PermissionError as e:
            if opt.remote:
                self.parent.shell.closeRemoteDisplay()

            print(e)
            print("Try to execute as root.\n")
            quit(1)


        # ------------------- Connect to Selectors in Main --------------------
        self.selector.register(self.df, selectors.EVENT_READ, self.readbuttons)
        self.log.debug('Registered %s with file no. %u to selector.',
                self.__class__.__name__, self.df.fileno())



    # -------------------------------------------------------------------------
    def readbuttons(self):
        """Reads the last event on the device.
        Sends the evaluated event to parentevent() and writes
        this to the log."""
        # ------------------- Readout Last Event on Device --------------------
        if not opt.dryrun:
            event = self.df.read(self.EVENT_SIZE)
            [time, value, type, number] = struct.unpack(self.MESSAGE_FORMAT,
                    event)


            # -------------------------- Output of Event ---------------------
            if type == EVENT_BUTTON:
                if value == 1:
                    self.log.info('keyevent:ENTER')
                    self.parent.buttonevent(eventtype.ENTER)
                else:
                    self.parent.buttonevent(eventtype.ENTER_R)

            elif type == EVENT_AXIS:
                if number == LEFT_RIGHT:
                    if value > 0:
                        self.log.info('keyevent:DOWN')
                        self.parent.buttonevent(eventtype.RIGHT)
                    elif value < 0:
                        self.log.info('keyevent:UP')
                        self.parent.buttonevent(eventtype.LEFT)

                elif number == TOP_DOWN:
                    if value > 0:
                        self.log.info('keyevent:RIGHT')
                        self.parent.buttonevent(eventtype.UP)
                    elif value < 0:
                        self.log.info('keyevent:LEFT')
                        self.parent.buttonevent(eventtype.DOWN)


#
