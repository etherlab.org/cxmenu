# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------



# ------------------------- Loading Necessary Modules -------------------------
import logging



# -----------------------------------------------------------------------------
class Cxmenu_module:
    """cxmenu application
    Module:      cxmenumodule

    This module describes the base class for all modules, that contains only
    the functionality to provide information for displaying.
    """

    # -------------------------------------------------------------------------
    showininterval = False

    # -------------------------------------------------------------------------
    def __init__(self):

        # ---------- Common for Logging
        self.log = logging.getLogger('cxmenu.' + __name__)
        self.log.setLevel(logging.DEBUG)

        self.submenu = None



    # -------------------------------------------------------------------------
    def getOutput(self):
        """Standard output getter for the display module.

        This method has to be overloaded by the child class. Otherwise the
        return is a hint for not available information is given.
        """

        self.log.debug('%s has no own getter for output.',
                self.__class__.__name__)

        return ['-', "N/A"]



    # -------------------------------------------------------------------------
    def getTitle(self):
        """Return the Title of the module.

        To keep this class compatible with the menu and its' entries a method
        getTitle() has to be implemented. In most cases the title of a module
        is equal to the first statement of its output.
        """
        return self.getOutput()[0]

#
