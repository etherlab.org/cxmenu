# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
import logging


# -----------------------------------------------------------------------------
class PufferHandler(logging.Handler):
    """cxmenu application
    Module:      logginghandler

    Logginghandler that puffers the logging messages to a list.
    """

    # -------------------------------------------------------------------------
    def __init__(self):
        """Initialise the the handler and the baseclass."""
        logging.Handler.__init__(self)
        self.puffer = []



    # -------------------------------------------------------------------------
    def emit(self, record):
        """Emits a record to an puffering array."""
        self.puffer.append(record)



    # -------------------------------------------------------------------------
    def readPuffer(self):
        """Returns the all stored information stored in the puffer.
        Afterwards the puffer array becomes cleared."""
        send = []
        for record in self.puffer:
            send.append(self.format(record))

        self.puffer.clear()

        return send

#
