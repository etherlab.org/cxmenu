# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
import getopt
import sys
"""cxmenu application
    Module:      option

    This skript reads out the given options and arguments and stores the
    information for global acces. Moreover this script catches wrong arguments
    and shows the usage text.
"""

longargs = "dryrun help remote verbose".split()

# ------------------------- List of Flags for Options -------------------------
dryrun = False  # In remote-mode without connection to a CX21xx
remote = False
verbose = False


# -----------------------------------------------------------------------------
def evalOpt():
    global dryrun, remote, verbose

    try:
        options, arguments = getopt.getopt(sys.argv[1:], 'dhrv', longargs)
    except getopt.GetoptError as e:
        print(e)
        usage()
        sys.exit(2)

    for [opt, arg] in options:
        if opt in ('-d', '--dryrun'):
            dryrun = True

            if not remote:
                remote = True

        if opt in ('-h', '--help'):
            usage()

        if opt in ('-r', '--remote'):
            remote = True

        if opt in ('-v', '--verbose'):
            verbose = True



# -----------------------------------------------------------------------------
def usage():
    """Interface and Menu for Beckhoff CX2100.
    Usage: cxmenu <option>

    Global options:
    -d --dryrun     Start the application without need of the display and
                    button driver. This option includes the remote mode.
    -h --help       Print this help.
    -r --remote     Enable an input via Keyboard and output on a remote.
    -v --verbose    Give an additional verbose output on the remote with
                    further information about the process."""

    print(usage.__doc__)
    exit(0)
