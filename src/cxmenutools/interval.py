# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------



# -----------------------------------------------------------------------------
class Interval():
    """cxmenu application
    Module:      interval

    This basic modul controls the output for displ the inteval information.
    """


    # -------------------------------------------------------------------------
    def __init__(self, parent, modules):
        self.parent = parent
        self.index = 0
        self.modules = modules



    # -------------------------------------------------------------------------
    def skip(self, step):
        self.index = (self.index + step) % len(self.modules)



    # -------------------------------------------------------------------------
    # Calls the output method of the imported modules, that are marked for the
    # interval output and returns the recived list. It adds some ':' to the
    # first statement to illustrate the heading in the output.
    def getOutput(self):
        stmt1, stmt2 = self.modules[self.index].getOutput()
        stmt1 = stmt1 + ':'
        return [stmt1, stmt2]



    # -------------------------------------------------------------------------
    def getCurrententry(self):
        return self.index
