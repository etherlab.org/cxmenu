# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""cxmenu application
    Module:      globals

    This globals skript contains information about the application, that are
    open for all modules.
"""



# ------------------------- Loading Necessary Modules -------------------------
from enum import Enum



# -----------------------------------------------------------------------------
# The dimension of the output display.
DSP_NUMLINES = 2
DSD_NUMCHARS = 16



# -----------------------------------------------------------------------------
# The standard timeout intervals
SLOWTIMEOUT = 10
FASTTIMEOUT = 2
MENUTIMEOUT = 15
CALLBACKTIMEOUT = 0.2



# -----------------------------------------------------------------------------
# An enumeration of all events that are handeld in this programm.
class eventtype(Enum):
    ENTER = 0
    UP = 1
    DOWN = 2
    LEFT = 3
    RIGHT = 4
    ENTER_R = 5


# -----------------------------------------------------------------------------
# An enumeration of kinds of event handling.
class eventhandle(Enum):
    HOLD = 0
    RELEASE = 1


# -----------------------------------------------------------------------------
# Set and get a reference to the mainfunction. This is necessary to give
# objects, that are not defined in this mainfunction, with methods of the main-
# function. For excample to set the timeout. An direct reference is only set
# for one instance with @parent. This parent reference is specified to one
# object.
mainref = None

def setMain(ref):
    global mainref
    mainref = ref



def getMainRef():
    if mainref is None:
        raise AttributeError('No given reference to mainfunction.')

    else:
        return mainref



# -----------------------------------------------------------------------------
# Tests whether an attribute is a list or a tuple and ensures that returns in
# every case as an list or something array-like.
def toList(input):

    if type(input) == list or type(input) == tuple:
        return input
    else:
        return [input]


# -----------------------------------------------------------------------------
# Set and get a reference to the selector.
selref = None

def setSelector(ref):
    global selref
    selref = ref


def getselector():
    if selref is None:
        raise AttributeError('No given reference to selector.')

    else:
        return selref
