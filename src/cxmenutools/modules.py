# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
import importlib
import logging
import os
import pkgutil
import types

import importlib.machinery as imp
import cxmenubuiltins



# -----------------------------------------------------------------------------
class Modules():
    """cxmenu application
    Module:      modules

    This basic modul loads all modules that are available in the corresponding
    repository. The modules are seperated in two lists. One with modules with
    information that are given cyclic an one with all Modlues.
    """


    # -------------------------------------------------------------------------
    def __init__(self, parent):
        self.parent = parent
        self.selector = parent.select
        self.indexintv = 0
        self.index = 0
        self.modules = []  # All modules.
        self.menuModules = []  # Only modules for menu and interacting.
        self.intervalmodules = []  # Interval is shown if nothing happens.

        self.log = logging.getLogger('cxmenu.' + __name__)
        self.log.setLevel(logging.DEBUG)


    # -------------------------------------------------------------------------
    # The repository, that contains all moduls gets paresd to crate lists for
    # the inerval and menu.
    def importmodules(self):

        usrmodpath = '/etc/cxmenu.d'
        path = []

        try:
            path = os.listdir(usrmodpath)
            self.log.debug('Path to modules: %s', usrmodpath)

        except FileNotFoundError:
            warnStr = 'No directory \'' + usrmodpath + \
                '\' for user specified modules.'
            self.log.warning(warnStr)


        # Loads in a first step the user specified modules. This ones will be
        #   used in any case.
        loadedModNames = []
        for fname in path:
            if fname.endswith(".py"):
                self.loadFromFile(usrmodpath, fname)
                loadedModNames.append(fname[:-3])

        # Loads in a second step the default modules if there is no overloaded
        #   one installed yet.
        for _, mod, ispkg in pkgutil.iter_modules(cxmenubuiltins.__path__):
            if mod not in loadedModNames:
                self.loadByImport('cxmenubuiltins', mod)



    # -------------------------------------------------------------------------
    def loadFromFile(self, usrmodpath, modName):
        self.log.debug('Load user-module: %', modName)

        module = os.path.join(usrmodpath, modName)
        loader = imp.SourceFileLoader("module", module)
        mod = types.ModuleType(loader.name)


        loader.exec_module(mod)
        try:
            cxmenuModule = mod.create()

        except AttributeError:
            # Deactivate module if empty.
            return

        self.modules.append(cxmenuModule)
        self.activateModule(cxmenuModule, mod)



    # -------------------------------------------------------------------------
    def loadByImport(self, modpath, module):
        self.log.debug('Load builtin-module: %s', module)
        mod = importlib.import_module(str(modpath) + '.' + str(module))
        cxmenuModule = mod.create()
        self.modules.append(cxmenuModule)

        self.activateModule(cxmenuModule, mod)



    # -------------------------------------------------------------------------
    def activateModule(self, cxMod, mod):

        # Collecting moduls for menu.
        if cxMod.submenu:
            self.log.debug("   -> Books this for Menu")
            self.menuModules.append(cxMod)

        # Collecting modules for interval.
        if cxMod.showininterval:
            self.log.debug("   -> Books this for Interval")
            self.intervalmodules.append(cxMod)



    # -------------------------------------------------------------------------
    # Returns a list with modules that are booked for the interval.
    def getIntervallist(self):
        return self.intervalmodules

    # -------------------------------------------------------------------------
    # Returns a list with modules that are booked for the menu.
    def getMenulist(self):

        return self.menuModules
