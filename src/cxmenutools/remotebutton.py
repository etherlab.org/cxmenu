# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
import logging
import selectors
import sys

from cxmenutools.globals import eventtype
from cxmenutools import option as opt



# -----------------------------------------------------------------------------
class RemoteButton():
    """cxmenu application
    Module:      remotebutton

    This class contains all methods to read the button events from a keyboard
    instead of the CX21xx buttons during remote-mode.
    """


    # -------------------------------------------------------------------------
    def __init__(self, parent, shell):
        # ---------- Common for logging
        self.log = logging.getLogger('cxmenu.' + __name__)
        self.log.setLevel(logging.DEBUG)

        # ---------- For remote control
        self.parent = parent
        self.selector = parent.select

        self.shell = shell
        self.input = []
        if opt.remote:
            try:
                self.selector.register(sys.stdin,
                        selectors.EVENT_READ,
                        self.readbuttons)

            except:
                self.log.error('Global exception.', exc_info = True)

            self.log.debug('Registered %s with file no. %u to selector.',
                self.__class__.__name__, sys.stdin.fileno())


    # -------------------------------------------------------------------------
    def readbuttons(self):
        self.input.append(self.shell.getch())

        while self.input[0] == 27 and len(self.input) <= 2:
            self.input.append(self.shell.getch())


        if self.input[0] == 27 and len(self.input) == 3:
            event = self.input[2]
            self.log.info('keyevent:' + str(event))
            self.input = []

            if event == 65:
                self.parent.buttonevent(eventtype.LEFT)

            elif event == 66:
                self.parent.buttonevent(eventtype.RIGHT)

            elif event == 67:
                self.parent.buttonevent(eventtype.UP)

            elif event == 68:
                self.parent.buttonevent(eventtype.DOWN)


        elif self.input.__contains__(-1) or len(self.input) > 3:
            self.input = []

        else:
            event = self.input[0]
            self.log.info('keyevent:' + str(self.input[0]))
            self.input = []

            if event == 10:
                self.parent.buttonevent(eventtype.ENTER)

            elif event == 127:
                self.parent.buttonevent(eventtype.ENTER_R)

            elif event == 113:  # 'q'
                raise KeyboardInterrupt


#
