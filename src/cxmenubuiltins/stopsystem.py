# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of the cxmenu application.
#
# The cxmenu application is free software: you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# The cxmenu application is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the cxmenu application. If not, see
# <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
from cxmenutools import menu
from cxmenutools import systemaction as sysact
from cxmenutools.cxmenumodule import Cxmenu_module


# --------------------------- Referenz to the Objekt --------------------------
def create():
    """Creates an object and returns a reference."""
    return Stopsystem()



# -----------------------------------------------------------------------------
class Stopsystem(Cxmenu_module):
    """cxmenu application
    Module:      stopsystem

    This module controls the save poweroff or reboot of the system.
    """
    showininterval = False



    # -------------------------------------------------------------------------
    def __init__(self):
        Cxmenu_module.__init__(self)

        # ------------- Create Instances to Pages of This Module --------------
        reboot = sysact.systemaction(self, 'Reboot', 'sudo reboot')
        poweroff = sysact.systemaction(self, 'Poweroff', 'sudo poweroff')

        menulist = [reboot, poweroff]
        self.submenu = menu.Menu(self, menulist)

        reboot.setMessage(['CX20xx', 'is rebooting...'])
        poweroff.setMessage(['CX20xx:', 'shutting down...'])



    # -------------------------------------------------------------------------
    def getOutput(self):
        stmt1 = "Reboot"
        stmt2 = ""

        return [stmt1, stmt2]

#
