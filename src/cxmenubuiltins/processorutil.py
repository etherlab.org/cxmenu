# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
from cxmenutools.cxmenumodule import Cxmenu_module
import os


# --------------------------- Referenz to the Objekt --------------------------
# With this create method an object of the modul is generatet and a reference
# to this is returned to collect this in a list.
def create():
    return Processorutil()



# -----------------------------------------------------------------------------
class Processorutil(Cxmenu_module):
    """cxmenu application
    Module:      processorutil

    This modul shows the current utilization of the CPU.
    """


    # -------------------------------------------------------------------------
    showininterval = True



    # -------------------------------------------------------------------------
    def __init__(self):
        Cxmenu_module.__init__(self)




    # -------------------------------------------------------------------------
    def getOutput(self):
        usage = os.popen("cat /proc/loadavg").read()
        usage = usage.split(' ', 1)
        stmt1 = "CPU Load"
        stmt2 = usage[0] + ' %'

        return [stmt1, stmt2]



    # -------------------------------------------------------------------------
    # No special options defined.
    def getOptions(self):
        return self.getOutput()
