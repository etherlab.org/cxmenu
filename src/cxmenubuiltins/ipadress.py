# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
# Get more information about socket on:
# https://docs.python.org/3/library/socket.html
from socket import gethostbyname, gethostname
from cxmenutools.cxmenumodule import Cxmenu_module



# --------------------------- Referenz to the Objekt --------------------------
# With this create method an object of the modul is generatet and a reference
# to this is returned to collect this in a list.
def create():
    return Ipadress()



# -----------------------------------------------------------------------------
class Ipadress(Cxmenu_module):
    """cxmenu application
    Module:      ipadress

    This module reads out the hostname and its ip adress.
    """


    # -------------------------------------------------------------------------
    showininterval = True



    # -------------------------------------------------------------------------
    def __init__(self):
        Cxmenu_module.__init__(self)




    # -------------------------------------------------------------------------
    # This output refreshes the written information by every call. This is
    # important to get the actual information of the ip adress without
    # restarting the programm.
    def getOutput(self):
        # TODO: It is necessary to display all existing interfaces. Therefore
        #       This module needs for ech interface an own page on the display.
        #       The functionality of multi-page output has to be implementern
        #       in general.
        stmt1 = "IP Adress"
        stmt2 = gethostbyname(gethostname())

        return [stmt1, stmt2]



    # -------------------------------------------------------------------------
    # No special options defined.
    def getOptions(self):
        return self.getOutput()


#
