# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------



# ------------------------- Loading Necessary Modules -------------------------
from cxmenutools.cxmenumodule import Cxmenu_module
from cxmenutools.page import Page
from cxmenutools import menu


# --------------------------- Referenz to the Objekt --------------------------
def create():
    """Create an object of the modul and return a reference."""
    return Remoteservice()



# -----------------------------------------------------------------------------
class Remoteservice(Cxmenu_module):
    """cxmenu application
    Module:      ipadress

    This module controls the remote service and shows its current status.
    """

    showininterval = True

    # -------------------------------------------------------------------------
    def __init__(self):
        Cxmenu_module.__init__(self)

        # ---------- Create Instances to Pages of This Module
        status = Page(self, "Status")
        handle = Page(self, "Handle")

        # ---------- Create a Submenu for This Module
        # menulist = [status.getTitle(), handle.getTitle() ]
        menulist = [status, handle]
        self.submenu = menu.Menu(self, menulist)



    # -------------------------------------------------------------------------
    def getOutput(self):
        stmt1 = "Remote service"
        stmt2 = "closed"  # TODO: Add correct output.

        return [stmt1, stmt2]


#
