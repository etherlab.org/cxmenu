# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------



# =============================================================================
# Class as interface to the cxmenu application.

# ------------------------- Loading Necessary Modules -------------------------
from cxmenutools import globals as glb
from cxmenutools.cxmenumodule import Cxmenu_module

import logging
import os
import pdcom
import selectors
from socket import socket, AF_INET, SOCK_STREAM
import time as t



# --------------------------- Referenz to the Objekt --------------------------
# With this create method an object of the modul is generatet and a reference
# to this is returned to collect this in a list.
def create():
    return Rtprocessutil()



# =============================================================================
class Rtprocessutil(Cxmenu_module):
    """cxmenu application
    Module: Rtprocessutil

    This module shows the current degree of capacity utilization of the
    connected real time processes.
    """

    showininterval = True

    # -------------------------------------------------------------------------
    def __init__(self):
        Cxmenu_module.__init__(self)

        # ---------- Common for Logging
        self.log = logging.getLogger('cxmenu.' + __name__)
        self.log.setLevel(logging.DEBUG)


        # ---------- Connect the process and tasks
        self.taskidx = 0
        self.tasks = []
        self.prcs = None


    # -------------------------------------------------------------------------
    def getOutput(self):
        stmt1 = 'RT Process Util'

        if self.prcs and self.prcs.isConnected:
            stmt2 = '%.2f %%' % (self.getUtil() * 100)

        else:
            stmt2 = 'No connection...'

        return [stmt1, stmt2]



    # -------------------------------------------------------------------------
    def connectedCallback(self):
        """Callback method can bind the subscriber to the process."""

        self.prcs.find('/Taskinfo/%u/Period' % (self.taskidx))



    # -----------------------------------------------------------------------------
    def findCallback(self, var):
        """Subscribe all existing tasks of the process.

        In case that there is more than one task in th process all of them
        become subscribed to read the util of each task.
        """
        if var:
            self.taskidx += 1
            self.tasks.append(Task(var.sampleTime))
            self.prcs.find('/Taskinfo/%u/Period' % (self.taskidx))
            # Variable gibt es, oder nicht.

        else:
            for idx in range(self.taskidx):
                self.prcs.subscribe(self.tasks[idx],
                    '/Taskinfo/%u/Period' % (idx),
                    self.tasks[idx].sampleTime, 1)
                self.prcs.subscribe(self.tasks[idx],
                    '/Taskinfo/%u/ExecTime' % (idx),
                    self.tasks[idx].sampleTime, 1)



    # -------------------------------------------------------------------------
    def getUtil(self):
        """Add the utilization of all tasks together.

        The utilization of the process is the sum of all working tasks. This
        method returns the cummulatice sum.
        """

        cumUtil = 0.0

        for task in self.tasks:
            cumUtil += task.lpUtil

        return cumUtil




# =============================================================================
class Process(pdcom.Process):
    """Interface to realtime process for usage in cxmenu."""


    # -------------------------------------------------------------------------
    def __init__(self, addr, parent, connectedCallback=None, sel=None):
        # ---------- Common
        self.parent = parent
        self.log = logging.getLogger('cxmenu.' + __name__)
        self.log.setLevel(logging.DEBUG)

        self.main = glb.getMainRef()


        # ---------- For the pdcom process
        self.addr = addr
        pdcom.Process.__init__(self)
        self.connectedCallback = connectedCallback

        self.socket = None
        self.fo = None

        self.selector = sel
        self.nextCall = t.time()
        self.main.registerCallback(self, self.run)

        self.isConnected = False



    # -----------------------------------------------------------------------------
    def getNextEventtime(self):
        """Return the timestamp to call the next event."""
        return self.nextCall



    # -------------------------------------------------------------------------
    def connect(self):
        """Connect the socket of realtime process."""

        # Make shure to close connection before reconnect the socket.
        self.connloss()

        self.log.debug('Connect the process %s.', self.addr)

        try:
            self.socket = socket(AF_INET, SOCK_STREAM)
            self.socket.connect(self.addr)

        except:
            self.log.debug('   -> Failed to connect to %s.', self.addr)
            self.connloss()
            return


        self.log.debug('   -> Connected socket to %s, fileno is %s.',
                self.addr,
                self.socket.fileno())
        self.fo = os.fdopen(self.socket.fileno(), "wb")

        if self.selector:
            self.log.debug('Registered %s with file no. %s to selector.',
                    self.__class__.__name__, self.fo)
            self.selector.register(self.fo, selectors.EVENT_READ,
                    self.dataToRead)
        self.nextCall = None



    # -------------------------------------------------------------------------
    def run(self):
        """Callback method for the selector.

        This method checks if the call by the selector is ment for this object.
        Only if the time accords to the set timestamp the connect method gets
        called.
        """

        if self.nextCall and t.time() >= self.nextCall:
            self.connect()



    # -------------------------------------------------------------------------
    def findReply(self, var):
        """Reimplemented from pdcom.Process.

        Is called by the python method process.find().
        """

        self.parent.findCallback(var)



    # -------------------------------------------------------------------------
    def dataToRead(self):
        """Callback method for the selector.

        The method asyncData() reads the information from the process. In case
        the connection to the process is interrupted a timeout fpr reconnection
        is set (for the selector).
        """
        if self.asyncData() <= 0:
            self.log.debug('Connection is interrupted...')
            self.isConnected = False
            self.connloss()



    # -------------------------------------------------------------------------
    def connloss(self):
        """Determine the timestamp for the next call and resets
        the selector."""

        self.nextCall = t.time() + glb.SLOWTIMEOUT
        self.log.debug('Try to (re)connect at: %s',
                t.strftime('%H:%M:%S', t.gmtime(self.nextCall)))

        if self.selector and self.fo:
            try:
                self.log.debug('Unregistering fd %s from selector.', self.fo)
                self.selector.unregister(self.fo)

            except:
                pass

        if self.fo:
            self.fo.close()
            self.fo = None

        self.socket = None
        self.reset()



    # -------------------------------------------------------------------------
    def connected(self):
        """Reimplemented from pdcom.Process (mandatory)

        This signal is received when protocol initialization has completed.
        """

        self.log.debug('Process is connected.')
        self.isConnected = True

        if self.connectedCallback:
            self.connectedCallback()



    # -------------------------------------------------------------------------
    def read(self, n):
        """Reimplemented from pdcom.Process (mandatory).

        Fetch data from input stream.
        """
        return self.socket.recv(n)


    # -------------------------------------------------------------------------
    def write(self, buf):
        """Reimplemented from pdcom.Process (mandatory)

        Send buf to output stream
        Return number of bytes written, which MUST be the whole buffer

        It is wise to use buffered output because the the handler calls
        write() very often.
        """
        self.fo.write(buf)
        return len(buf)



    # -------------------------------------------------------------------------
    def flush(self):
        """Reimplemented from pdcom.Process (mandatory)

        Flush output stream.
        Return 0 on success
        """

        self.fo.flush()
        return 0    # Flush is always successful




# ============================================================================
class Task(pdcom.Subscriber):
    """Subscriber of a pdcom process. Known as data channel"""

    # -------------------------------------------------------------------------
    def __init__(self, sampleTime):
        # ---------- Common for logging
        self.log = logging.getLogger('cxmenu.' + __name__)
        self.log.setLevel(logging.DEBUG)


        # ---------- For the subscriber
        pdcom.Subscriber.__init__(self)
        self.exec = None
        self.period = None
        self.util = 0

        # ---------- Lowpass filter
        self.lpUtil = 0
        self.sampleTime = sampleTime
        self.tau = 3.0



    # -------------------------------------------------------------------------
    def newGroupValue(self, time):
        """Receive the information summarized in an group (mandatory)

        To group information the subscriber needs a common index.
        """

        if self.exec and self.period and self.period.getValue() > 0:
            self.util = self.exec.getValue() / self.period.getValue()
            self.lowpass(self.util)



    # -------------------------------------------------------------------------
    def newValue(self, subscpt):
        """Receive the information of a channel / subscriber (mandatory)"""
        pass



    # -------------------------------------------------------------------------
    def lowpass(self, util):
        self.lpUtil += self.sampleTime / self.tau * (util - self.lpUtil)



    # -------------------------------------------------------------------------
    def active(self, path, subscpt):

        self.log.debug('%s is active.', path)

        if path.endswith('Period'):
            self.period = subscpt

        else:
            self.exec = subscpt



    # -------------------------------------------------------------------------
    def invalid(self, path, id):

        self.log.debug('%s is invalid.', path)
        self.exec = None
        self.period = None



#
