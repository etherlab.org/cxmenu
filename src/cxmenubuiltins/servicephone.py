# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------


# ------------------------- Loading Necessary Modules -------------------------
from cxmenutools.cxmenumodule import Cxmenu_module


# --------------------------- Referenz to the Objekt --------------------------
def create():
    """Create an object of the modul and return a reference."""

    return Servicephone()

# -----------------------------------------------------------------------------
class Servicephone(Cxmenu_module):
    """cxmenu application
    Module:      servicephone

    This module contains the phone number to call the IgH-Support.
    """

    showininterval = True


    # --------------------------- Konstruktoraufruf ---------------------------
    def __init__(self):
        Cxmenu_module.__init__(self)



    # -------------------------------------------------------------------------
    def getOutput(self):
        stmt1 = "Support"
        stmt2 = "+49 201 36014 0"

        return [stmt1, stmt2]



#
