#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2020  Jonathan Grahl <gn@igh.de>
#
# This file is part of cxmenu.
#
# Cxmenu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Cxmenu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cxmenu.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

# Create script and package with: 'python3 setup.py install'
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()


setuptools.setup(
    name="igh-cxmenu",
    version="0.3",
    author="Jonathan Grahl",
    author_email="jonathan.grahl@igh.de",
    packages=setuptools.find_packages(),
    scripts=["cxmenu"],

    description="A package to control the input and output of the cx21xx menu",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://sourceforge.net/projects/cxmenu/",
    license="GNU General Public License v3 (GPLv3)",
    classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
            "Operating System :: POSIX :: Linux",
    ],
    python_requires='>=3.6',
)
